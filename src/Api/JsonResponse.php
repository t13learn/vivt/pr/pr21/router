<?php

namespace Api;

class JsonResponse
{
    protected $response = [
        'status' => false,
        'errors' => [],
        'data' => [],
        'time' => [],
    ];

    protected $time = 0;

    public function __construct()
    {
        $this->time = microtime(true);
    }

    public function setStatus(bool $status): self
    {
        $this->response['status'] = $status;
        return $this;
    }

    public function setErrors(array $errors): self
    {
        $this->response['errors'] = $errors;
        return $this;
    }

    public function setData(array $data): self
    {
        $this->response['data'] = $data;
        return $this;
    }

    public function sendError(string $field, string $message): void
    {
        $this->setData([]);
        $this->setStatus(false);
        $this->setErrors([$field => $message]);
        $this->send();
    }

    public function setTime(string $type, $time): void
    {
        $this->response['time'][$type] = $time;
    }

    public function send(): void
    {
        $this->setTime('total', microtime(true) - $this->time);
        header('Content-Type: application/json');
        echo json_encode($this->response);
        die();
    }
}