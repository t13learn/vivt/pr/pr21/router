<?php

namespace Api;

use Dummy\City;

class LocationController extends AbstractController
{
    protected $City;

    public function __construct()
    {
        parent::__construct();
        $this->City = new City();
    }

    public function get(): array
    {
        $city = $this->City->getCurrentCity();

        $this->status = (bool)$city;

        return $city;
    }

    public function select(): array
    {
        $cityId = (int)$this->request->get('city_id');

        if (!$cityId) {
            $this->errors['city_id'] = 'Не передан идентификатор города';
            return [];
        }

        $isExist = $this->City->hasCity($cityId);
        if (!$isExist) {
            $this->errors['city_id'] = 'Не найден город с идентификатором: ' . $cityId;
            return [];
        }

        $city = $this->City->selectCity($cityId);
        $this->status = (bool)$city;

        return $city;
    }

    public function list(): array
    {
        $cityList = [];
        foreach ($this->City->getAllCity() as $data) {
            $city = [];
            $city['id'] = (int)$data['ID'];
            $city['name'] = (string)$data['NAME'];
            $city['displayName'] = (string)$data['DISPLAY_NAME'];
            $cityList[] = $city;
        }

        $this->status = (bool)$cityList;

        return $cityList;
    }
}