<?php

namespace Api;

interface ControllerInterface
{
    public function getErrors(): array;

    public function getStatus(): bool;

    public function setError(string $field, string $message): self;
}