<?php

namespace Api;

use Dummy\HttpRequest;

class ApiController
{
    const cache = '/.cache.php';
    /**
     * @var JsonResponse
     */
    protected $response;
    /**
     * @var HttpRequest
     */
    protected $request;
    protected $controllerList = [];
    protected $mapFile = '';
    protected $cacheFile = '';
    protected $isCacheValid = false;
    protected $time = 0;

    public function __construct()
    {
        $this->response = new JsonResponse();
        $this->request = new HttpRequest();
        //запоминаем время старта
        $this->time = microtime(true);
    }

    public function useCache(string $file): self
    {
        //проверяем что файл конфига маршрутов существует
        if (!file_exists($file)) {
            throw new \Exception('file not found: ' . pathinfo($file, PATHINFO_BASENAME));
        }

        $this->mapFile = $file;

        //вычисляем имя файла кеша конфига
        $this->cacheFile = dirname($file) . self::cache;

        if (!file_exists($this->cacheFile)) {
            return $this;
        }

        //если кеш существует - загружаем его
        $cache = (array)include $this->cacheFile;

        //проверям что версии файла конфига и файла роутера соответствуют кешу
        $this->isCacheValid = $cache['revision']['base'] === filemtime(__FILE__) && $cache['revision']['map'] === filemtime($file);
        if ($this->isCacheValid) {
            //если кеш актуален - используем его
            $this->controllerList = $cache['map'];
        }

        return $this;
    }

    public function register(string $path, string $controller): self
    {
        if ($this->isCacheValid) {
            //если кеш актуален - регистрировать маршруты нет необходимости
            return $this;
        }

        //проверяем что метод не зарегистрирован
        if (array_key_exists($path, $this->controllerList)) {
            throw new \Exception('path ' . $path . ' already registered!');
        }
        //регистрируем связку метод-контроллер в списке маршрутов
        $this->controllerList[$path] = $controller;
        //реализация паттерна chaining
        return $this;
    }

    public function dispatch(): void
    {
        //логика роутера
        $this->validateRouteMap(); //проверяем список маршрутов

        $path = $this->getRequestedPath(); //получаем метод из запроса
        $controller = $this->getController($path); //получаем контроллер

        $action = $this->getAction($path); //получаем метод контроллера

        //вычисляем время работы роутера
        $startTime = microtime(true);
        $this->response->setTime('route', $startTime - $this->time);

        //логика базового контроллера
        $actionResult = [];
        try {
            //вызываем метод контроллера
            $actionResult = (array)$controller->$action();
        } catch (\Exception $exception) {
            //ловим ошибки контроллера
            $controller->setError('action', (string)$exception->getMessage());
        }

        //наполняем ответ даннымми, ошибками, таймингами
        $this->response->setErrors($controller->getErrors());
        $this->response->setData($actionResult);
        $this->response->setStatus($controller->getStatus());
        //вычисляем время работы целевого контроллера
        $this->response->setTime('logic', microtime(true) - $startTime);
        //отправляем запрос
        $this->response->send();
    }

    protected function getAction(string $path): string
    {
        //возаращаем последнюю часть пути
        return pathinfo($path, PATHINFO_FILENAME);
    }

    protected function getRequestedPath(): string
    {
        //получаем запрашиваемый URI
        $path = $this->request->getRequestedPage();
        $path = str_replace('/api/', '/', $path); //вырезаем путь к каталогу
        $path = str_replace('/index.php', '', $path); //вырезаем имя скрипта
        return $path; //остается относительный путь к методу
    }

    protected function validateRouteMap(): void
    {
        if ($this->isCacheValid) {
            //если кеш актуален - проверять маршруты нет необходимости
            return;
        }

        //если кеш не актуален - проверяем маршруты перед генерацией кеша
        foreach ($this->controllerList as $path => $controller) {
            $this->validateRoute($path, $controller);
        }

        //если кеш не актуален - генерируем его
        if ($this->cacheFile) {
            $cache = [];
            //записываем в кеш версию роутера
            $cache['revision']['base'] = filemtime(__FILE__);
            //записываем в кеш версию конфига маршрутов
            $cache['revision']['map'] = filemtime($this->mapFile);
            //записываем в кеш список маршрутов
            $cache['map'] = $this->controllerList;
            //генерируем файл кеша
            file_put_contents($this->cacheFile, '<?php' . PHP_EOL . 'return ' . var_export($cache, true) . ';');
        }
    }

    protected function validateRoute(string $path, string $controller): void
    {
        $reflectionClass = new \ReflectionClass($controller);

        //проверяем, что контроллер следует интерфейсу ControllerInterface
        if (!$reflectionClass->implementsInterface(ControllerInterface::class)) {
            throw new \Exception('controller ' . $controller . ' must be implements interface "' . ControllerInterface::class . '"');
        }

        $method = $this->getAction($path);

        //проверяем, что контроллер имеет целевой метод для текущего маршрута
        if (!$reflectionClass->hasMethod($method)) {
            throw new \Exception('controller ' . $controller . ' must have a method "' . $method . '"');
        }

        $reflectionMethod = $reflectionClass->getMethod($method);

        //проверяем, что целевой метод контроллера является публичным
        if (!$reflectionMethod->isPublic()) {
            throw new \Exception('controller ' . $controller . ' must have a public method "' . $method . '", not private/protected!');
        }

        //проверяем, что целевой метод контроллера возвращает в качестве результата массив
        if (!$reflectionMethod->hasReturnType() || (string)$reflectionMethod->getReturnType() !== 'array') {
            throw new \Exception('controller ' . $controller . ' must have a method "' . $method . '" with return value is "array"!');
        }
    }

    protected function getControllerClass(string $path): string
    {
        //извлекаем имя целевого контроллера из списка маршрутов
        return (string)$this->controllerList[$path];
    }

    protected function getController(string $path): ControllerInterface
    {
        //ищем имя класса целевого контроллера
        $controllerClass = $this->getControllerClass($path);
        if (!$controllerClass) {
            $this->response->sendError('path', 'unknown api method ' . $path);
        }
        try {
            //создаем экземпляр контроллера
            $controller = new $controllerClass();
        } catch (\Throwable $e) {
            $this->response->sendError('path', 'bad controller for api method ' . $path);
        }

        return $controller; //возвращаем экземпляр целевого контроллера
    }
}