<?php

namespace Api;

use Dummy\HttpRequest;

abstract class AbstractController implements ControllerInterface
{
    /**
     * @var HttpRequest
     */
    protected $request;
    protected $errors = [];
    protected $status = false;

    public function __construct()
    {
        $this->request = new HttpRequest();
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    public function getStatus(): bool
    {
        return $this->status;
    }

    public function setError(string $field, string $message): ControllerInterface
    {
        $this->status = false;
        $this->errors[$field] = $message;
        return $this;
    }

    protected function error(string $field, string $message): array
    {
        $this->setError($field, $message);
        return [];
    }
}