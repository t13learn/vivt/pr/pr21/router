<?php

namespace Dummy;

class City
{
    protected const cityList = [
        0 => [
            'ID' => 0,
            'NAME' => 'Москва',
            'DISPLAY_NAME' => 'город Москва',
        ],
        1 => [
            'ID' => 1,
            'NAME' => 'Питер',
            'DISPLAY_NAME' => 'город Санкт-Петербург',
        ],
    ];

    protected const sessionKey = 'city_id';

    public function hasCity(int $id): bool
    {
        return array_key_exists($id, $this->getAllCity());
    }

    public function getAllCity(): array
    {
        return self::cityList;
    }

    public function getCurrentCity(): array
    {
        return (array)self::cityList[$this->getCityId()];
    }

    public function selectCity(int $id): array
    {
        $this->setCityId($id);

        return $this->getCurrentCity();
    }

    protected function getCityId(): int
    {
        return (int)$_SESSION[self::sessionKey];
    }

    protected function setCityId(int $id): void
    {
        $_SESSION[self::sessionKey] = $id;
    }

}