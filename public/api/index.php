<?php
require('bootstrap.php');

use Api\ApiController;
use Api\LocationController;

(new ApiController())
    ->useCache(__FILE__)
    ->register('/Location/get', LocationController::class)
    ->register('/Location/select', LocationController::class)
    ->register('/Location/list', LocationController::class)
    ->dispatch()
;